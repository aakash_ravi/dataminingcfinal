#include "CountryFunctions.h"
#include "naiveBayesContainer.h"
#include "kMeansFunctions.h"
#include <sstream>

#ifndef VECTOR
#include <vector>
#endif

int main(int argc, char * argv[]) {
	
	std::vector<MCD_CSTR> xmlPaths;

	int index=1;

	for (index=1; index<=argc-1; index++) {
		xmlPaths.push_back(argv[index]);
	}

	doubleMatrix newDataVector;

	try {
		newDataVector=computeDataMatrix(xmlPaths);
	}
	catch(int e) {
		std::cout << "Error processing your xml paths, please check to make sure that the path is correct" << std::endl;
		return 0;	
	}

	std::cout << "Please input a function you want to run on your data" << std::endl;

	std::string desiredFunction;
	std::cin >> desiredFunction;

	if (desiredFunction=="kMeans") {

		//perform kMeans Here

		int numDimensions=xmlPaths.size();

		int numIterations;

		std::cout << "Please input the number of iterations k means should run. WARNING: THE HIGHER THE NUMBER THE LONGER THE COMPUTATION WILL TAKE" << std::endl;

		std::cin >> numIterations;


		//need to handle non numbers.. 

		while ((!std::cin) || numIterations<0) {
			std::cin.clear();
			std::cout << "Incorrect input! Make sure you input a proper positive integer." << std::endl;
			std::cin >> numIterations;
		}

		std::cout << "Please input centroid vectors, components separated by a space, and the vectors themselves separated by a new line. When done input \"END\"" << std::endl;

		std::string newLine;
		centroidMatrix centroidVec;

		while (std::getline(std::cin, newLine)) {

			if (newLine=="END") break;


			std::stringstream stringStream(newLine);
			std::vector<char> newCharVec;
			std::string centroidChar;

			if (newLine!="") {
				if (newLine.size()!=(numDimensions*2)-1) { //handle case where input is the wrong length
					std::cout << "Wrong size of input, the length of your centroids has to correspond to the number of dimensions." << std::endl;
				}
				else {
					while (stringStream >> centroidChar) {
						if (centroidChar=="+" || centroidChar=="-" || centroidChar=="0") {
							newCharVec.push_back(*centroidChar.c_str());
						}
						else {
							std::cout << "Please input proper centroid alignments: characters \'+\', \'-\', and \'0\' are accepted" << std::endl; 
							break;
						}
					}
				}
				
				if (newCharVec.size()!=0) {
					centroidVec.push_back(newCharVec);
				}
			}
		}

		kMeansContainer<double> * newContainer= new kMeansContainer<double>(numDimensions, newDataVector, centroidVec);
		newContainer->computeGroups(numIterations);
		
		std::cout << "STATISTICS:" << std::endl;


		for (int centroidIndex=0; centroidIndex<=newContainer->centroids.size()-1; centroidIndex++) {

			int counter=0;
			for (std::vector<std::shared_ptr<vectors<double>>>::iterator itr=newContainer->container.begin(); itr!=newContainer->container.end(); ++itr) {
			if ((**itr).getGroup()==centroidIndex)
				counter++;
			}
			std::cout << "Centroid " << centroidIndex << " " << "has " << counter << " " << "vectors clustered around it." << std::endl;
		}

		delete newContainer;
		getchar();
		return 0;
	}

	else if (desiredFunction=="naiveBayes") {

		//perform naiveBayes here

		//get number of dimensions

		int numDimensions=xmlPaths.size(); 

		//get classifier index

		int groupIndex;

		std::cout << "Please input the dimension (i.e. which component) you want to run the classifier on" << std::endl;

		std::cin >> groupIndex;


		//need to handle non numbers.. 

		while ((!std::cin) || groupIndex<0 || groupIndex>numDimensions-1) {
			std::cin.clear();
			std::cout << "Incorrect input! Make sure that your component is within the dimensions of your vector space." << std::endl;
			std::cin >> groupIndex;
		}

		naiveBayesContainer<double> * newBayesContainer=new naiveBayesContainer<double>();
		std::vector<double> groupValues;

		groupValues=newBayesContainer->createAndTrainModel(newDataVector, groupIndex);
		std::cout << "Your group classifiers are" << std::endl;

		for (std::vector<double>::iterator itr=groupValues.begin(); itr!=groupValues.end(); ++itr) {
			std::cout << *itr << " " ;
		}
		
		std::cout << std::endl;
		std::cout << "You have 2 options for running data against the model:" << std::endl;
		std::cout << "Option 1: Given an input vector of other attributes, and a specific classification, the model will determine the probability that these attributes belong to this specific classification. Warning: Numbers may become very messy if they are large." << std::endl;
		std::cout << "Option 2: Given an input vector of other attributes, the model will determine the proper classification this input vector belongs to based on which classification has the highest probability." << std::endl;
		std::cout << "Please choose your option (1/2)" << std::endl;

		int optionInt;
	
		std::cin >> optionInt;

		while ((!std::cin) && (optionInt!=1) && (optionInt!=2)) {
			std::cin.clear();
			std::cout << "Please input a proper option." << std::endl;
			std::cin >> optionInt;
		}

		if (optionInt==1) {
			std::cout << "Please input a vector of attributes, remember the length of the vector must be one less than the number of dimensions, we are going to predict what the final attribute will be. The vector components should be separated by spaces." << std::endl;
			std::string newLine;
			std::getline(std::cin, newLine);
			std::stringstream newStream(newLine);

			//need to make sure that the input vector is of the right length

			std::stringstream tempStream(newLine);
			int wordCounter=0;
			std::string tempString;

			while (tempStream >> tempString) wordCounter++;
			
			while (wordCounter!=numDimensions-1) {
					std::cout << "Incorrect number of components." << std::endl;
					std::getline(std::cin, newLine);
					newStream.clear(); newStream << newLine;
					tempStream.clear(); tempStream << newLine;
					wordCounter=0; tempString.clear();
					while (tempStream >> tempString) wordCounter++;
			}

			std::vector<double> newPoint;
			std::string temp;

			while (newStream >> temp) {
				newPoint.push_back(atoi(temp.c_str()));
			}

			int groupNumber;
			std::cout << "Please input a classification that you would like to test your point on." << std::endl;
			std::cin >> groupNumber;

			while ((!std::cin) || groupNumber>groupValues.size()-1 || groupNumber<0) {
				std::cout << "Incorrect classification number, you need to choose a proper classification corresponding to the number of groups there are." << std::endl;
				std::cin >> groupNumber;
			}

			std::cout << "The probability of this vector belonging to this classification is " << newBayesContainer->runModel(newPoint, groupNumber) << std::endl;
			std::cout << "Remember that this probability is relative and its absolute value should not be taken too literally." << std::endl;
			delete newBayesContainer;
			getchar();
			return 0;
		}

		else if(optionInt==2) {
			std::cout << "Please input a vector of attributes, remember the length of the vector must be one less than the number of dimensions, we are going to predict what the final attribute will be. The vector components should be separated by spaces." << std::endl;
			std::string newLine;
			std::getline(std::cin, newLine);
			std::stringstream newStream(newLine);

			//need to make sure that the input vector is of the right length

			std::stringstream tempStream(newLine);
			int wordCounter=0;
			std::string tempString;

			while (tempStream >> tempString) wordCounter++;
			
			while (wordCounter!=numDimensions-1) {
					std::cout << "Incorrect number of components." << std::endl;
					std::getline(std::cin, newLine);
					newStream.clear(); newStream << newLine;
					tempStream.clear(); tempStream << newLine;
					wordCounter=0; tempString.clear();
					while (tempStream >> tempString) wordCounter++;
			}
			

			std::vector<double> newPoint;
			std::string temp;

			while (newStream >> temp) {
				newPoint.push_back(atoi(temp.c_str()));
			}

			std::vector<double> results=newBayesContainer->runMultipleModelsOnPoint(newPoint);

			std::vector<double>::iterator max=std::max_element(results.begin(), results.end());

			int counter=0;
			for (std::vector<double>::iterator itr=results.begin(); itr!=results.end(); ++itr) {
				if (itr==max)
					break;
				else
					counter++;
			}

			std::cout << "The model classifies this vector in classification bucket " << counter << std::endl;
			getchar();
			return 0;
		}

		else {
			std::cout << "Incorrect Option!" << std::endl;
			getchar();
			delete newBayesContainer;
			return 0;
		}
		

	}

		
	else {
		std::cout << "Please input a valid function" << std::endl;
		return 0;
	}
}
