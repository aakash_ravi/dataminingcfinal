/***********NOTES ON PROJECT & COMPILATION**********\

-include contains all the required header files and class definitions

-classFiles contains all the class implementations 

-sampleDataSets contains samples of the XML format the program expects.
XML files of this type are freely available for many datasets at: http://datacatalog.worldbank.org/

-supporting documentation of the source is provided in the MS Word file.

-main.cpp contains the main module that will launch in the CLI. Please make sure to compile the source files 
and make sure all header dependencies are in the proper directory of the compilation unit so that main.cpp can see and use them.

Enjoy and please ask if you have any questions :)
