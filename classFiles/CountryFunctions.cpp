#include "stdafx.h"
#include "CountryFunctions.h"


std::map<std::string, double> computeCountries(MCD_CSTR xmlPath, bool AVERAGE) {
	std::map<std::string, double> countryVector;
	std::map<std::string, int> numberOfEntriesVector;
	
	CMarkup xml2;

	bool fileLoadedWithoutProblems=true;
	
	fileLoadedWithoutProblems=xml2.Load(xmlPath);

	if (fileLoadedWithoutProblems==false) {
		throw xmlPathNotAvailable;
	}

	//after error handling, we dive into the xml

	xml2.FindElem();
	xml2.IntoElem(); //into root


	MCD_CSTR newString("name");
	std::string countryName;
	boolean takeData;

	xml2.FindElem();
	xml2.IntoElem(); // go into data..

	while (xml2.FindElem()) { //while we find some records
		xml2.IntoElem(); //into record
		xml2.FindElem("field"); //entered first field

		while (xml2.GetAttrib(newString)!="Value") { //if we dont' see the value field go to next field
			if (xml2.GetAttrib(newString)=="Country or Area") {
				countryName=xml2.GetAttrib("key");
				xml2.FindElem();
			}

			else if (xml2.GetAttrib(newString)=="Year") {
				if ((atoi(xml2.GetData())>=1990))
					takeData=true;
				else 
					takeData=false;
				xml2.FindElem();
			}

			else {
				xml2.FindElem();
			}
		}
		
		if (takeData) {
			if (countryVector.find(countryName)==countryVector.end()) {
				countryVector.insert(std::make_pair(countryName, atoi(xml2.GetElemContent())));
				numberOfEntriesVector.insert(std::make_pair(countryName, 1));
			}

			else {
				if (AVERAGE==true) {
					numberOfEntriesVector.find(countryName)->second+=1;
					countryVector.find(countryName)->second+=atoi(xml2.GetElemContent());
				}
				else 
					countryVector.find(countryName)->second+=atoi(xml2.GetElemContent());
			}
		}

		xml2.OutOfElem(); //go out of the current record to the next record
	}

	for (std::map<std::string, double>::iterator countryIterator=countryVector.begin(); countryIterator!=countryVector.end(); ++countryIterator) {
		countryIterator->second=countryIterator->second/(numberOfEntriesVector.find(countryName)->second); //take average
	}
	
	std::map<std::string, double>::iterator itr=countryVector.begin(); //delete any values that have zero..this implies there was never any data on them
	while (itr != countryVector.end()) {
		if (itr->second==0) {
			countryVector.erase(itr++);
		} else {
			++itr;
		}
	}

	return countryVector;
}


std::vector<std::vector<double>> computeDataMatrix(std::vector<MCD_CSTR> xmlPaths) {

	std::vector<std::vector<double>> returnVector;

	for (int i=0; i<=xmlPaths.size()-1; i++) { //fill output vector with some dummy vectors before we input real values
		std::vector<double> dummyVec;
		returnVector.push_back(dummyVec);
	}

	std::vector<std::map<std::string, double>> intermediateMap; 

	for (int i=0; i<=xmlPaths.size()-1; i++) { //create an intermediate map for preprocessing of data

			try {
				intermediateMap.push_back(computeCountries(xmlPaths[i],true));
			}
			catch (int e) {
				throw xmlPathNotAvailable;
				return returnVector;
			}

		}

	for (std::map<std::string, double>::iterator mapIterator=intermediateMap.begin()->begin(); mapIterator!=intermediateMap.begin()->end(); ++mapIterator) {
		bool dataNotAvailable=false;
		
		for (int i=0; i<=intermediateMap.size()-1; i++) { //check to make sure this current country data is available in all the data sets
			if ((intermediateMap[i].find(mapIterator->first)!=intermediateMap[i].end()))
				continue;
			else {
				dataNotAvailable=true;
				break;
			}
		}

		if (!dataNotAvailable) { //if all data is available push back the new vector of data values.. 
			for (int i=0; i<=intermediateMap.size()-1; i++) {
				returnVector[i].push_back(intermediateMap[i].find(mapIterator->first)->second);
			}
		}

	}


	return returnVector;
}
