#include <vector>
#include <math.h>

template <class T>
class vectors: public std::vector<T> {
	int _group; //keeps track of what group the vector is in in k means algorithm (what centroid it's closest too)
public:
	double currentMinDistance; //keeps track of what the minimum distance from its closest centroid is
	int getGroup() {return _group;}
	void setGroup(int newVal) {_group=newVal;}

	double computeDistance(std::shared_ptr<vectors> otherVec);

	vectors(): _group(0) { currentMinDistance=0;};
	~vectors();
	vectors(const vectors & srcVec);
	vectors & vectors::operator =(const vectors & srcVector);
};

template <typename T>
double vectors<T>::computeDistance(std::shared_ptr<vectors<T>> otherVec) 
	{
		int size=otherVec->size(), euclidianDistance=0, index=0;
		while(index<size) {
		euclidianDistance+=std::pow(((*this)[index]-(*otherVec)[index]),2);
		index++;
		}
		return sqrt((double)euclidianDistance);
	};

template <typename T>
vectors<T>::~vectors() {
};

template <typename T>
vectors<T>::vectors(const vectors<T> & srcVector) {
	for (std::vector<T>::const_iterator index=srcVector._values.begin(); index!=srcVector._values.end(); ++index) 
		this->push_back(*index);
	_group=srcVector._group;
};

template <typename T>
vectors<T> & vectors<T>::operator=(const vectors<T> & srcVector) {
	if (this==&srcVector) {
			return *this;
		}
		else {
			this->values=srcVector._values;
			this->group=srcVector._group;
		}
		return *this;
};
