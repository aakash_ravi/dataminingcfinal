#include "naiveBayesFunctions.h"
#include <stdlib.h>

template <class T>
class naiveBayesContainer {
public:
	typedef typename std::vector<std::vector<T>> matrix;

	/*constructors*/
	naiveBayesContainer(): trained(false){};
	naiveBayesContainer(const naiveBayesContainer & srcContainer);
	naiveBayesContainer<T> & naiveBayesContainer<T>::operator=(const naiveBayesContainer & srcContainer);
	~naiveBayesContainer();
	/*constructors*/

	/*members*/
	bool trained; // false if model hasn't been trained yet
	matrix meanMatrix;
	matrix deviationMatrix;
	std::vector<double> groupProbabilities;
	/*members*/

	void trainModel(matrix testPoints, std::vector<T> groupValues, int groupIndex);
	std::vector<T> createAndTrainModel(matrix testPoints, int groupIndex); 

	double runModel(std::vector<T> data, int group); //test probability of a certain vector to belong to a certain group.
	std::vector<double> runMultipleModelsOnPoint(std::vector<T> dataPoint); //test the probability of a single vector on various groups
};

template <typename T>
naiveBayesContainer<T>::naiveBayesContainer(const naiveBayesContainer & srcContainer) {
	trained=srcContainer.trained;
	std::copy(srcContainer.meanMatrix.begin(), srcContainer.meanMatrix.end(), std::back_inserter(meanMatrix));
	std::copy(srcContainer.deviationMatrix.begin(), srcContainer.deviationMatrix.end(), std::back_inserter(deviationMatrix));
	std::copy(srcContainer.groupProbabilities.begin(), srcContainer.groupProbabilities.end(), std::back_inserter(groupProbabilities));
};

template <typename T>
naiveBayesContainer<T> & naiveBayesContainer<T>::operator=(const naiveBayesContainer & srcContainer) {
	if (this==&srcContainer)
		return *this;
	else {
		meanMatrix=srcContainer.meanMatrix;
		deviationMatrix=srcContainer.deviationMatrix;
		trained=srcContainer.trained;
		groupProbabilities=srcContainer.groupProbabilities;
		return *this;
	}

};

template <typename T>
naiveBayesContainer<T>::~naiveBayesContainer() {};

template <typename T>
void naiveBayesContainer<T>:: trainModel(matrix testPoints, std::vector<T> groupValues, int groupIndex) {
	
	//add zero to group values if not present
	if (groupValues[0]!=0) {
		groupValues.insert(groupValues.begin(),0);
	}

	//clean test points into proper format

	matrix cleanedTestPoints;

	for (int i=0; i <= testPoints[0].size()-1; i++) {
			std::vector<T> newVec;
			for (int j=0; j <= testPoints.size()-1; j++) {
				newVec.push_back(testPoints[j][i]);
			}
			cleanedTestPoints.push_back(newVec);
	}

	//train the model
	matrix newDataPointContainer=addGroups(cleanedTestPoints, groupValues, groupIndex);


	groupProbabilities=probabilityOfGroups(newDataPointContainer, groupValues);
	meanMatrix=computeMeanOfGroups(newDataPointContainer, groupValues.size(), cleanedTestPoints[0].size(), groupIndex);
	deviationMatrix=computeStandardDeviationGroups(meanMatrix,newDataPointContainer, groupValues.size(), cleanedTestPoints[0].size(), groupIndex);

	trained=true;


};

template <typename T>
double naiveBayesContainer<T>::runModel(std::vector<T> data, int group) {

   if (trained)
	return computeProbability(meanMatrix,deviationMatrix,groupProbabilities, data, group);
   else {
	   std::cout << "Please train the model first before running it on data";
	   exit(EXIT_FAILURE);
	   getchar();
   }

};

template <typename T>
std::vector<T> naiveBayesContainer<T>::createAndTrainModel(matrix testPoints, int groupIndex) {

	//find 25th, 50th, and 75th percentile for our classification "buckets"
	std::vector<T> percentileVector=computePercentiles(testPoints[groupIndex]);

	//clean test points into proper format
	matrix cleanedTestPoints;

	for (int i=0; i <= testPoints[0].size()-1; i++) {
			std::vector<T> newVec;
			for (int j=0; j <= testPoints.size()-1; j++) {
				newVec.push_back(testPoints[j][i]);
			}
			cleanedTestPoints.push_back(newVec);
	}

	//add zero to percentileVector if not present
	if (percentileVector[0]!=0) {
		percentileVector.insert(percentileVector.begin(),0);
	}

	//train the model

	matrix newDataPointContainer=addGroups(cleanedTestPoints, percentileVector, groupIndex);


	groupProbabilities=probabilityOfGroups(newDataPointContainer, percentileVector);
	meanMatrix=computeMeanOfGroups(newDataPointContainer, percentileVector.size(), cleanedTestPoints[0].size(), groupIndex);
	deviationMatrix=computeStandardDeviationGroups(meanMatrix,newDataPointContainer, percentileVector.size(), cleanedTestPoints[0].size(), groupIndex);

	trained=true;

	return percentileVector;

}; 

template <typename T>
std::vector<double> naiveBayesContainer<T>::runMultipleModelsOnPoint(std::vector<T> dataPoint) {

	std::vector<double> vectorOfProbabilitiesForPoint;

	if (trained) {

		int index=0;
		while (index<groupProbabilities.size()) { //populate the vector with initial values according to each group
			vectorOfProbabilitiesForPoint.push_back(0);
			index++;
		}

		index=0;
		for (index=0; index<groupProbabilities.size(); index++) {
			vectorOfProbabilitiesForPoint[index]=computeProbability(meanMatrix,deviationMatrix,groupProbabilities, dataPoint, index); //find probability of dataPoint being in a specific classification given by the index
		}

		return vectorOfProbabilitiesForPoint;
	}
	else {
		std::cout << "Please train the model first before running it on data";
		exit(EXIT_FAILURE);
		getchar();
	}

}
