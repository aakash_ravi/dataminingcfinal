#ifndef KMEANSFUNCTIONS_H
#define KMEANSFUNCTIONS_H

#include <memory>
#include "vector.h"


typedef std::vector<std::vector<char>> centroidMatrix;

template <class T>
class kMeansContainer {
public:
	typedef typename std::vector<std::vector<T>> matrix;

	int dimensions; //keeps track of # of dimensions the vectors
	std::vector < std::shared_ptr<vectors<T>>> container;
	std::vector<std::shared_ptr<vectors<T>>> centroids;

	/*constructors*/
	kMeansContainer(int numberOfDimensions): dimensions(numberOfDimensions) {}; 
	kMeansContainer(int numberOfDimensions, matrix dataPoints, centroidMatrix centroidChoice);
	~kMeansContainer();
	kMeansContainer(const kMeansContainer<T> & srcContainer);
	kMeansContainer<T> & kMeansContainer<T>::operator=(const kMeansContainer<T> & srcContainer);
	/*constructors*/

	void computeGroups(int computationLength);
	void computeVectorGroups(bool groupChangeIndicator);
	void computeCentroidRelabeling();
};

template <typename T>
std::vector<T> computePercentiles(std::vector<T> dataVector);

#include <array>

template<typename T>
void kMeansContainer<T>::computeGroups(int computationLength) {

	bool groupChangeIndicator=true;
	int index=0;
	while ((groupChangeIndicator) && (index<computationLength)) { //put a cap on the max number of iterations to limit computation time 
		computeVectorGroups(groupChangeIndicator);
		if (groupChangeIndicator) {  //avoid unecessary recomputation of centroidRelabelling when groups didn't change
		computeCentroidRelabeling(); 
		}
		else
			break;
		index++;
	}

};

template <typename T>
void kMeansContainer<T>::computeVectorGroups(bool groupChangeIndicator) {
	std::size_t currentVectorIndex=0, currentCentroidIndex=0;
	double minimumDistance=0;
	std::vector<std::shared_ptr<vectors<T>>>::const_iterator centroidIterator;
	groupChangeIndicator=false;

	while (currentVectorIndex<container.size()) {
		currentCentroidIndex=0;
		for(centroidIterator=centroids.begin(); centroidIterator!=centroids.end(); ++centroidIterator) {
			//initialize minimumDistance and do initial centroid clustering
			if (centroidIterator==centroids.begin()) {
				minimumDistance=((container[currentVectorIndex]->computeDistance(*centroidIterator))); 
				if ((container[currentVectorIndex]->currentMinDistance!=0) && (minimumDistance>container[currentVectorIndex]->currentMinDistance)) {
					minimumDistance=container[currentVectorIndex]->currentMinDistance;
				}
				else {
					container[currentVectorIndex]->currentMinDistance=minimumDistance;
					container[currentVectorIndex]->setGroup(currentCentroidIndex);
					groupChangeIndicator=true;
				}
			}
			//change centroid group if we find a centroid that's closer
			else {
				double otherDistance=((container[currentVectorIndex]->computeDistance(*centroidIterator))); 
				if (minimumDistance>otherDistance) {
					minimumDistance=otherDistance;
					container[currentVectorIndex]->setGroup(currentCentroidIndex);
					container[currentVectorIndex]->currentMinDistance=minimumDistance;
					groupChangeIndicator=true;
				}
			}
			currentCentroidIndex++;
		}
		currentVectorIndex++;
	}

};

template <typename T>
void kMeansContainer<T>::computeCentroidRelabeling() {
	std::vector<std::shared_ptr<vectors<T>>>::const_iterator vectorIterator;
	std::vector<double> mean;
	int initializeMeanArray=0;
	for (initializeMeanArray=0; initializeMeanArray<dimensions; initializeMeanArray++) {  
		mean.push_back(0);
	}

	std::size_t currentCentroidIndex, numberInGroup, dimensionIndex;
	
	for (currentCentroidIndex=0; currentCentroidIndex<centroids.size(); currentCentroidIndex++) {
		numberInGroup=0;
		for (initializeMeanArray=0; initializeMeanArray<dimensions; initializeMeanArray++) {  //initialize array holding mean with dummy values 0 at first and then change during loop
			mean[initializeMeanArray]=0;
		}
		for (vectorIterator=container.begin(); vectorIterator!=container.end(); ++vectorIterator) {
			if ((**vectorIterator).getGroup()==currentCentroidIndex) {
				for (dimensionIndex=0; dimensionIndex<dimensions; dimensionIndex++) {
					mean[dimensionIndex]+=(**vectorIterator)[dimensionIndex];
				}
				numberInGroup++;
			}
		}
		for (dimensionIndex=0; dimensionIndex<dimensions; dimensionIndex++) {
			if (numberInGroup!=0) {
			(*centroids[currentCentroidIndex])[dimensionIndex]=(mean[dimensionIndex]/numberInGroup); //take arithmetic mean to minimize centroids' euclidian distance from surrounding vectors
			}
		}

		}
	};

template <typename T>
kMeansContainer<T>::~kMeansContainer() {
	/*while (container.size()!=0) {
		delete container[0];
	}
	while (centroids.size()!=0) {
		delete centroids[0];
	}*/
};

template <typename T>
kMeansContainer<T>::kMeansContainer(const kMeansContainer<T> & srcContainer) {
	std::copy(srcContainer.container.begin(), srcContainer.container.end(), std::back_inserter(container));
	std::copy(srcContainer.centroids.begin(), srcContainer.centroids.end(), std::back_inserter(centroids));
	dimensions=srcContainer.dimensions;
};

template <typename T>
kMeansContainer<T> & kMeansContainer<T>::operator=(const kMeansContainer<T> & srcContainer) {
	if (this==&srcContainer)
		return *this;
	else {
		container=srcContainer.container;
		centroids=srcContainer.centroids;
		dimensions=srcContainer.dimensions;
		return *this;
	}

};

template <typename T>
std::vector<T> computePercentiles(std::vector<T> dataVector) {

	std::sort(dataVector.begin(), dataVector.end());
	int firstQuarterPoint=dataVector.size()/4;
	int medianPoint=dataVector.size()/2;
	int thirdQuarterPoint=medianPoint+firstQuarterPoint;

	T firstQuarterPercentile=dataVector[firstQuarterPoint];
	T medianPercentile=dataVector[medianPoint];
	T thirdQuarterPercentile=dataVector[thirdQuarterPoint];

	std::vector<T> dataStatistics;
	dataStatistics.push_back(firstQuarterPercentile);
	dataStatistics.push_back(medianPercentile);
	dataStatistics.push_back(thirdQuarterPercentile);
	return dataStatistics;
};

template <typename T>
kMeansContainer<T>::kMeansContainer(int numOfDimensions, matrix dataPoints, centroidMatrix centroidChoice) {

		this->dimensions=numOfDimensions;
		
		for (int i=0; i <= dataPoints[0].size()-1; i++) {
			std::shared_ptr<vectors<T>> newVec(new vectors<T>());
			for (int j=0; j <= dataPoints.size()-1; j++) {
				newVec->push_back(dataPoints[j][i]);
			}
			this->container.push_back(newVec);
		}


		for (int i=0; i <= centroidChoice.size()-1; i++) {
			std::shared_ptr<vectors<T>> newVec(new vectors<T>());
			/*vectors * newVec=new vectors();*/
			for (int j=0; j <= centroidChoice[i].size()-1; j++) {
				std::vector<double> statisticsVector=computePercentiles(dataPoints[j]);
				switch (centroidChoice[i][j]) {
				case ('+'):
					newVec->push_back(statisticsVector[2]);
					break;
				case ('-'):
					newVec->push_back(statisticsVector[0]);
					break;
				default:
					newVec->push_back(statisticsVector[1]);
				}
			}
			this->centroids.push_back(newVec);
		}
	};



#endif

#include <vector>
#include <array>
#include <iostream>

static const double inv_sqrt_2pi = 0.3989422804014327;

typedef std::vector<std::vector<double>> doubleMatrix;

double gaussian_density(double data, double mean, double standard_deviation);

doubleMatrix addGroups(doubleMatrix oldDataPointContainer, std::vector<double> groupValues, int groupIndex);

std::vector<double> probabilityOfGroups (doubleMatrix newDataPointContainer, std::vector<double> groupValues);

doubleMatrix computeMeanOfGroups (doubleMatrix newDataPointContainer, int numOfGroups, int numOfDimensions, int groupIndex);

doubleMatrix computeStandardDeviationGroups (doubleMatrix meanMatrix, doubleMatrix newDataPointContainer, int numOfGroups, int numOfDimensions, int groupIndex);

double computeProbability (doubleMatrix meanMatrix, doubleMatrix deviationMatrix, std::vector<double> groupProbability, std::vector<double> dataPoint, int group);