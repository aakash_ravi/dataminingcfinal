#include "Markup.h"
#include <iostream>
#include <ostream>
#include <fstream>
#include <string>
#include <map>
#include <vector>

#ifndef COUNTRYFUNCTIONS_H
#define COUNTRYFUNCTIONS_H

const int xmlPathNotAvailable=15; //define error code for an invalid path to XML file

std::map<std::string, double>  computeCountries(MCD_CSTR xmlPath, bool AVERAGE=true);

std::vector<std::vector<double> > computeDataMatrix(std::vector<MCD_CSTR> xmlPaths);

#endif